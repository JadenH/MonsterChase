﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameTypes;
using GridSystem;
using UnityEngine.UI;

public class Player : MyGameObject
{
    public UILogic.Options Options;
    public bool Turn;
    private GameObject _thePowerups;
    public int MoveMultiplier = 1;
    public List<Powerup> Powerups;
	// Update is called once per frame
	public void Update () {
        if (Turn)
        {
            int horizontal = (int)Input.GetAxis("Horizontal");
            int vertical = (int)Input.GetAxis("Vertical");
            if (vertical < 0 || vertical > 0 || horizontal < 0 || horizontal > 0)
            {
                foreach (Powerup powerup in Powerups)
                {
                    powerup.DoPowerUp();
                }
                if (vertical != 0) { horizontal = 0; }
                if (horizontal != 0) { vertical = 0; }
                int zAngle = getTurningAngle(vertical, horizontal);

                Vector2 currentCell = Grid.getCellIndex(gameObject.transform.position);
                Vector2 newCell = new Vector2(currentCell.x + horizontal * MoveMultiplier, currentCell.y + vertical * MoveMultiplier);
                WrapAround(ref newCell);

                transform.position = Grid.GetCellCenter(newCell);
                PositionIndex = newCell;
                transform.rotation = Quaternion.Euler(0f, 0f, zAngle);
                Turn = false;
            }
        }
	}

    void WrapAround(ref Vector2 newCell)
    {
        if (newCell.x > Options.SizeX) { newCell.x = 0; }
        if (newCell.x < 0) { newCell.x = Options.SizeX; }
        if (newCell.y > Options.SizeY) { newCell.y = 0; }
        if (newCell.y < 0) { newCell.y = Options.SizeY; }
    }
}
