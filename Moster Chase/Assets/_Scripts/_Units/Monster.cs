﻿using System;
using System.Collections.Generic;
using GameTypes;
using GridSystem;
using UnityEngine;
using Random = UnityEngine.Random;

public class Monster : MyGameObject
{
    public bool Turn;

    protected static Dictionary<Direction, Vector2> _direction = new Dictionary<Direction, Vector2>()
    {
        {Direction.North, new Vector2(0,1)},
        {Direction.East, new Vector2(1,0)},
        {Direction.South, new Vector2(0,-1)},
        {Direction.West, new Vector2(-1,0)},
    };   

    public void Start()
    {
        _logic = FindObjectOfType<GameLogic>();
        SetPosition();
    }

    public void SetPosition()
    {
        if (_player == null)
        {
            _player = FindObjectOfType<Player>();
        }
        Vector2 position = new Vector2(Random.Range(0, _logic.Options.SizeX + 1), Random.Range(0, _logic.Options.SizeY + 1));
        while (_logic.CellHasObject(position) || position == _player.PositionIndex)
        {
            position = new Vector2(Random.Range(0, _logic.Options.SizeX + 1), Random.Range(0, _logic.Options.SizeY + 1));
        }
        _logic.GridObjects.Add(position, gameObject);
        PositionIndex = position;
        transform.position = Grid.GetCellCenter(PositionIndex);
    }

    public void Update()
    {
        if (Turn)
        {
            Pathfind();
            Turn = false;
        }
    }

    public void Pathfind()
    {
        if (_player == null)
        {
            _player = FindObjectOfType<Player>();
        }
        Vector2 playerCell = Grid.getCellIndex(_player.transform.position);
        Vector2 currentCell = Grid.getCellIndex(transform.position);
        Vector2 closestCell = currentCell;
        _logic.GridObjects.Remove(currentCell);
        float zAngle = 0f;
        foreach (Direction direction in Enum.GetValues(typeof(Direction)))
        {
            if (Vector2.Distance(currentCell + _direction[direction], playerCell) <
                Vector2.Distance(closestCell, playerCell))
            {
                if (!_logic.CellHasObject(currentCell + _direction[direction]))
                {
                    closestCell = currentCell + _direction[direction];
                    zAngle = getTurningAngle(direction);
                }
            }
        }
        _logic.GridObjects.Add(closestCell, gameObject);
        PositionIndex = closestCell;
        transform.rotation = Quaternion.Euler(0f, 0f, zAngle);
        transform.position = Grid.GetCellCenter(closestCell);
    }
}
