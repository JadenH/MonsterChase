﻿using UnityEngine;
using System.Collections;
using GridSystem;

public class Powerup : MyGameObject
{
    private int turn;
    private bool active;
    private int multiplier;
	// Use this for initialization
	void Start () 
    {
        _logic = FindObjectOfType<GameLogic>();
        SetPosition();
	    multiplier = Random.Range(2, 4);
	    if (multiplier == 3)
	    {
	        GetComponent<SpriteRenderer>().color = new Color(0,250f,0);
	    }
    }

	// Update is called once per frame
    private void Update()
    {
        if (position == _player.PositionIndex)
        {
            _player.Powerups.Add(this);
            gameObject.SetActive(false);
        }
    }

    public void DoPowerUp()
    {
        if (turn < 5)
        {
            _player.MoveMultiplier = multiplier;
            turn++;
        }
        else
        {
            _player.MoveMultiplier = 1;
            Destroy(this);
        }
    }
}

