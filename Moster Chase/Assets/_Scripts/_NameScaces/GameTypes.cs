﻿using UnityEngine;
using System.Collections;

namespace GameTypes
{
    public enum UnitType
    {
        Player,
        Monster
    }

    public enum Direction
    {
        North,
        East,
        South,
        West,
    }

}