﻿using UnityEngine;
using System.Collections;
using GameTypes;
using GridSystem;

public class MyGameObject : MonoBehaviour {

    protected Player _player;
    protected Vector2 position;
    protected GameLogic _logic;
    public Vector2 PositionIndex;

    public void SetPosition()
    {
        if (_player == null)
        {
            _player = FindObjectOfType<Player>();
        }
        position = new Vector2(Random.Range(0, _logic.Options.SizeX + 1), Random.Range(0, _logic.Options.SizeY + 1));
        while (_logic.CellHasObject(position) || position == _player.PositionIndex)
        {
            position = new Vector2(Random.Range(0, _logic.Options.SizeX + 1), Random.Range(0, _logic.Options.SizeY + 1));
        }
        _logic.GridObjects.Add(position, gameObject);
        PositionIndex = position;
        transform.position = Grid.GetCellCenter(PositionIndex);
    }

    protected float getTurningAngle(Direction direction)
    {
        switch (direction)
        {
            case Direction.North:
                return 0f;
            case Direction.South:
                return 180f;
            case Direction.West:
                return 90f;
            case Direction.East:
                return 270f;
        }
        return 0f;
    }

    protected int getTurningAngle(int x, int y)
    {
        if (x != 0)
        {
            if (x > 0)
            {
                return 0;
            }
            return 180;
        }
        if (y != 0)
        {
            if (y > 0)
            {
                return 270;
            }
            return 90;
        }
        return 0;
    }
}
