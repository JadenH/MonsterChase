﻿using System.Collections;
using System.Collections.Generic;
using GridSystem;
using UnityEngine;
using UnitySampleAssets._2D;

public class GameLogic : MonoBehaviour
{

    public GameObject MonsterPrefab;
    public GameObject PowerupPrefab;
    public GameObject GridPrefab;
    public UILogic GameUI;
    public UILogic.Options Options;


    private GameObject[] _gameMonsters;
    private GameObject _theMonsters;
    private GameObject[] _Powerups;
    private GameObject _thePowerups;

    /*private GameObject[] _Powerups;
    private GameObject _thePowerups;*/

    private Player _player;
    public Dictionary<Vector2, GameObject> GridObjects = new Dictionary<Vector2, GameObject>();
    private GameObject _gridGameObject;

    private int WinCounter;
    private int CountMoves;

    public bool CellHasObject(Vector2 index)
    {
        return GridObjects.ContainsKey(index);
    }

    //private int _phase = -1;
	// Use this for initialization
	void Start ()
	{
        //Create our Player
	    _player = FindObjectOfType<Player>();
	}

    void CreateMonsterPool(int size)
    {
        if (_theMonsters != null)
        {
            foreach (GameObject monster in _gameMonsters)
            {
                Destroy(monster);
            }
            Destroy(_theMonsters);
        }
        _theMonsters = new GameObject {name = "Monsters"};
        _gameMonsters = new GameObject[size];
        for (int i = 0; i < size; i++)
        {
            _gameMonsters[i] = (GameObject)Instantiate(MonsterPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            _gameMonsters[i].transform.parent = _theMonsters.transform;
            _gameMonsters[i].name = "Monster";
        }
    }

    void CreatePowerups(int size)
    {
        if (_Powerups != null)
        {
            foreach (GameObject powerup in _Powerups)
            {
                Destroy(powerup);
            }
            Destroy(_thePowerups);
        }
        _thePowerups = new GameObject { name = "Powerups" };
        _Powerups = new GameObject[size];
        for (int i = 0; i < size; i++)
        {
            _Powerups[i] = (GameObject)Instantiate(PowerupPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            _Powerups[i].transform.parent = _thePowerups.transform;
            _Powerups[i].name = "Powerup";
        }
    }

    public void StartGame(UILogic.Options options)
    {
        Options = options;
        _player.Options = Options;
        _gameMonsters = new GameObject[0];
        //_Powerups = new GameObject[0];
        Vector2 cellIndex = new Vector2(Options.SizeX/2, Options.SizeY/2);
        _player.transform.position = Grid.GetCellCenter(cellIndex);
        _player.PositionIndex = cellIndex;
        CreateMonsterPool(options.MonsterCount);
        CreatePowerups(3);
        Vector2 centerPos = Grid.GetCellCenter(new Vector2(Options.SizeX / 2, Options.SizeY / 2));
        Camera.main.transform.position = new Vector3(centerPos.x, centerPos.y, -10);
        Camera.main.orthographicSize = Mathf.Max(Options.SizeX, Options.SizeY) / 2 + 2;
        _gridGameObject = new GameObject()
        {
            name = "GridObjects"
        };
        CreateGrid();
        GameUI.WinCounterCanvas.gameObject.SetActive(true);
        CountMoves = 0;
        WinCounter = (int)(Options.SizeX * Options.SizeY) / 4;
        if (WinCounter < 2)
        {
            WinCounter = 2;
        }
        else if (WinCounter > 20)
        {
            WinCounter = 20;
        }
        PlayerTurn();
    }

    private void CreateGrid()
    {
        for (int x = 0; x <= Options.SizeX; x++)
        {
            for (int y = 0; y <= Options.SizeY; y++)
            {
                GameObject temp = (GameObject)Instantiate(GridPrefab, Grid.GetCellCenter(new Vector2(x, y)), Quaternion.identity);
                temp.transform.parent = _gridGameObject.transform;
            }
        }
    }
	// Update is called once per frame
    void PlayerTurn()
    {
        WinCounting();
        if (Lose())
        {
            RunLose();
        }
        else if (CountMoves == WinCounter)
        {
            Debug.Log("You Win!");
            RunWin();
        }
        else
        {
//            Camera.main.GetComponent<Camera2DFollow>().enabled = true;
//            Camera.main.transform.position = _player.transform.position;
//            Camera.main.orthographicSize = 6f;

            _player.Turn = true;
            StartCoroutine(MonsterTurn());
            CountMoves += 1;
        }
    }

    IEnumerator MonsterTurn()
    {
        while (_player.Turn)
        {
            yield return new WaitForSeconds(.1f);
        }
        foreach (GameObject gameMonster in _gameMonsters)
        {
            gameMonster.GetComponent<Monster>().Turn = true;
        }
        yield return new WaitForSeconds(1f);
        PlayerTurn();
    }

    private void RunLose()
    {
        GameUI.GameOverCanvas.gameObject.SetActive(true);
        GameUI.Background.SetActive(true);
        GridObjects.Clear();
        Destroy(_gridGameObject);
    }

    private void RunWin()
    {
        GameUI.WinGameCanvas.gameObject.SetActive(true);
        GameUI.Background.SetActive(true);
        GridObjects.Clear();
        Destroy(_gridGameObject);
    }

    private void WinCounting()
    {
        GameUI.MovesTaken.text = CountMoves.ToString();
        GameUI.MovesToWin.text = WinCounter.ToString();
    }

    private bool Lose()
    {
        foreach (GameObject gameMonster in _gameMonsters)
        {
            if (gameMonster.GetComponent<Monster>().PositionIndex == _player.PositionIndex)
            {
                Debug.Log("Game Over!");
                return true;
            }
        }
        return false;
    }

}
