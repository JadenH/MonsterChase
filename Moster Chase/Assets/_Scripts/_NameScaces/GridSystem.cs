﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GridSystem
{
    public static class Grid
    {
        public static int cellSize = 1;

        public static Vector2 getCellPos(Vector2 index)
        {
            return new Vector2(index.x * cellSize, index.y * cellSize);
        }

        public static Vector2 GetCellCenter(Vector2 index)
        {
            float cellHalf = Convert.ToSingle(cellSize) / 2;
            Vector2 cellPos = getCellPos(index);
            return new Vector2(cellPos.x + cellHalf, cellPos.y + cellHalf);
        }

        public static Vector2 getCellIndex(Vector2 position)
        {
            return new Vector2(Mathf.FloorToInt(position.x / cellSize), Mathf.FloorToInt(position.y / cellSize));
        }

        public static GameObject getNodeBuilding(Vector2 position)
        {
            //return Nodes[position.x][position.y].Building;
            return null;
        }
    }
}
