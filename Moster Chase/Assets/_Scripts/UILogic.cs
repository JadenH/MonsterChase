﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UILogic : MonoBehaviour
{
    public RectTransform GameSettingsRect;

    public InputField MonsterCountField;
    public InputField SizeXField;
    public InputField SizeYField;

    public Text ErrorMessenger;
    public Text MovesTaken;
    public Text MovesToWin;

    public Canvas GameOverCanvas;
    public Canvas SettingsCanvas;
    public Canvas WinGameCanvas;
    public Canvas WinCounterCanvas;

    public GameObject Background;
    [Serializable]
    public class Options
    {
        public int SizeX;
        public int SizeY;
        public int MonsterCount;
    }

    void UpdateSettings()
    {
        GUIStyle errorStyle = new GUIStyle()
        {
            richText = true,
            alignment = TextAnchor.MiddleCenter,
            fontSize = 16,
            wordWrap = true
        };
        Options options = new Options();

        string errorMessage = "";

        //TryParse is a Unity function that tries to get an integer out of the string. "out" is a reference to memory.
        int.TryParse(SizeXField.text, out options.SizeX);
        int.TryParse(SizeYField.text, out options.SizeY);
        int.TryParse(MonsterCountField.text, out options.MonsterCount);
        options.SizeX -= 1;
        options.SizeY -= 1;
        //If our player enter values that leave no room for monsters to spawn, lets correct them!
        if (options.MonsterCount < 1)
        {
            MonsterCountField.text = "1";
            errorMessage += "Must have at least 1 monster! ";
        }
        if (options.SizeX * options.SizeY - 9 - options.MonsterCount <= 0)
        {
            errorMessage += "No Possible Spawn Locations! Try increasing the map size.";
        }
        
        //Check if we have any error messages, if we don't -- lets play!
        if (errorMessage == "")
        {
            GameLogic logic = FindObjectOfType<GameLogic>();
            logic.StartGame(options);
            Background.SetActive(false);
            SettingsCanvas.gameObject.SetActive(false);
        }
        else
        {
            ErrorMessenger.gameObject.SetActive(true);
            ErrorMessenger.text = errorMessage;
            float textSize = errorStyle.CalcHeight(new GUIContent(errorMessage), 300);
            GameSettingsRect.sizeDelta =  new Vector2(300, 275 + textSize);
        }
    }

    public void Restart()
    {
        SettingsCanvas.gameObject.SetActive(true);
        GameOverCanvas.gameObject.SetActive(false);
        WinGameCanvas.gameObject.SetActive(false);
        WinCounterCanvas.gameObject.SetActive(false);
    }
}
